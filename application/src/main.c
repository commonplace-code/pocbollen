#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/sys/printk.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/pwm.h>
#include <zephyr/drivers/sensor.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/gap.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/addr.h>
#include <zephyr/bluetooth/services/bas.h>


#define UUID_W32	0x36010000
#define UUID_W2	 	0x49af
#define UUID_W3		0xaa0e
#define UUID_W48	0x49e5554dc34c

#define UUID_U16_BOKSBOL_SERVICE 		0x0000
#define UUID_U16_BOKSBOL_ACCELL_CHAR	0x7001

#define BT_UUID_BOKSBOL_SERVICE_VAL	\
	BT_UUID_128_ENCODE(0x12341523, 0x1234, 0xefde, 0x1523, 0x785feabcd123)

static const struct bt_uuid_128 boksbol_service_uuid =
	BT_UUID_INIT_128(BT_UUID_BOKSBOL_SERVICE_VAL);

static const struct bt_uuid_128 boksbol_acc_x_uuid = BT_UUID_INIT_128(
	BT_UUID_128_ENCODE(UUID_W32, 0x1234, 0xefde, 0x7001, UUID_W48));

static const struct bt_uuid_128 boksbol_acc_y_uuid = BT_UUID_INIT_128(
	BT_UUID_128_ENCODE(UUID_W32, 0x1234, 0xefde, 0x7002, UUID_W48));

static const struct bt_uuid_128 boksbol_acc_z_uuid = BT_UUID_INIT_128(
	BT_UUID_128_ENCODE(UUID_W32, 0x1234, 0xefde, 0x7003, UUID_W48));

#define COMPANY_ID_CODE 0xFFFF

static ssize_t read_accel_x(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset);
static ssize_t read_accel_y(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset);
static ssize_t read_accel_z(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset);

static uint8_t acc_notify_enabled[3] = {0};

static void ccc_cfg_changed_x(const struct bt_gatt_attr *attr, uint16_t value)
{
	acc_notify_enabled[0] = (value == BT_GATT_CCC_NOTIFY) ? 1 : 0;
}

static void ccc_cfg_changed_y(const struct bt_gatt_attr *attr, uint16_t value)
{
	acc_notify_enabled[1] = (value == BT_GATT_CCC_NOTIFY) ? 1 : 0;
}

static void ccc_cfg_changed_z(const struct bt_gatt_attr *attr, uint16_t value)
{
	acc_notify_enabled[2] = (value == BT_GATT_CCC_NOTIFY) ? 1 : 0;
}

BT_GATT_SERVICE_DEFINE(boksbol_attrs,
	BT_GATT_PRIMARY_SERVICE(&boksbol_service_uuid),
	BT_GATT_CHARACTERISTIC(&boksbol_acc_x_uuid.uuid,
			       BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
			       BT_GATT_PERM_READ,
			       read_accel_x, NULL, NULL),
	BT_GATT_CCC(ccc_cfg_changed_x, BT_GATT_PERM_READ|BT_GATT_PERM_WRITE),
	BT_GATT_CHARACTERISTIC(&boksbol_acc_y_uuid.uuid,
			       BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
			       BT_GATT_PERM_READ,
			       read_accel_y, NULL, NULL),
	BT_GATT_CCC(ccc_cfg_changed_y, BT_GATT_PERM_READ|BT_GATT_PERM_WRITE),
	BT_GATT_CHARACTERISTIC(&boksbol_acc_z_uuid.uuid,
			       BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
			       BT_GATT_PERM_READ,
			       read_accel_z, NULL, NULL),
	BT_GATT_CCC(ccc_cfg_changed_z, BT_GATT_PERM_READ|BT_GATT_PERM_WRITE),
);


typedef struct adv_mfg_data {
	uint16_t company_id;
} adv_mfg_data_t;

static struct bt_le_adv_param *adv_param =
	BT_LE_ADV_PARAM((BT_LE_ADV_OPT_CONNECTABLE | BT_LE_ADV_OPT_USE_IDENTITY),
		BT_GAP_ADV_FAST_INT_MIN_1,
		BT_GAP_ADV_FAST_INT_MAX_1,
		NULL);

static adv_mfg_data_t adv_mfg_data = { COMPANY_ID_CODE };

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME) - 1)

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
	BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_BOKSBOL_SERVICE_VAL),
	BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
};

static const struct bt_data sd[] = {
	BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
};

#define PWM_STEPS (50u)

static const struct pwm_dt_spec led_red = PWM_DT_SPEC_GET(DT_ALIAS(pwm_led_red));
static const struct pwm_dt_spec led_green = PWM_DT_SPEC_GET(DT_ALIAS(pwm_led_green));
static const struct pwm_dt_spec led_blue = PWM_DT_SPEC_GET(DT_ALIAS(pwm_led_blue));

static const struct gpio_dt_spec button = GPIO_DT_SPEC_GET_OR(DT_ALIAS(button), gpios, {0});
static struct gpio_callback button_cb_data;

struct bt_conn *my_conn = NULL;

static void on_security_changed(struct bt_conn *conn, bt_security_t level,
			     enum bt_security_err err)
{
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	if (!err) {
		printk("Security changed: %s level %u\n", addr, level);
	} else {
		printk("Security failed: %s level %u err %d\n", addr, level, err);
	}
}

static bool le_param_req(struct bt_conn *conn, struct bt_le_conn_param *param)
{
	//If acceptable params, return true, otherwise return false.
	return true;
}

// Callback for BLE parameter update
static void le_param_updated(struct bt_conn *conn, uint16_t interval, uint16_t latency, uint16_t timeout)
{
	struct bt_conn_info info;
	char addr[BT_ADDR_LE_STR_LEN];

	if(bt_conn_get_info(conn, &info))
	{
		printk("Could not parse connection info\n");
	}
	else
	{
		bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

		printk("Connection parameters updated!	\n\
		Connected to: %s						\n\
		New Connection Interval: %u				\n\
		New Slave Latency: %u					\n\
		New Connection Supervisory Timeout: %u	\n"
		, addr, info.le.interval, info.le.latency, info.le.timeout);
	}
}


void on_connected(struct bt_conn *conn, uint8_t err)
{
	if (err) {
		printk("Connection failed (err %u)\n", err);
		return;
	}
	printk("Connected\n");
	my_conn = bt_conn_ref(conn);

	pwm_set_pulse_dt(&led_blue, led_blue.period);
}

void on_disconnected(struct bt_conn *conn, uint8_t reason)
{
	printk("Disconnected (reason %u)\n", reason);
	if (my_conn) {
		bt_conn_unref(my_conn);
		my_conn = NULL;
	}
	pwm_set_pulse_dt(&led_blue, 0);
}

struct  bt_conn_cb conn_callbacks = {
	.connected = on_connected,
	.disconnected = on_disconnected,
	.le_param_req = le_param_req,
	.le_param_updated = le_param_updated,
	.security_changed = on_security_changed,
};

double accel_xyz[3];

static void acc_fetch(const struct device *sensor)
{
	static unsigned int count;
	struct sensor_value accel[3];
	struct sensor_value temperature;
	const char *overrun = "";
	int rc = sensor_sample_fetch(sensor);

	++count;

	if (rc == -EBADMSG) {
		/* Sample overrun.  Ignore in polled mode. */
		if (IS_ENABLED(CONFIG_LIS2DH_TRIGGER)) {
			overrun = "[OVERRUN] ";
		}
		rc = 0;
	}

	if (rc == 0) {
		rc = sensor_channel_get(sensor,
					SENSOR_CHAN_ACCEL_XYZ,
					accel);
	}

	if (rc < 0) {
		printk("ERROR: Update failed: %d\n", rc);
	} else {
		accel_xyz[0] = sensor_value_to_double(&accel[0]);
		accel_xyz[1] = sensor_value_to_double(&accel[1]);
		accel_xyz[2] = sensor_value_to_double(&accel[2]);

		//printk("#%u @ %u ms: %sx %f , y %f , z %f", count, k_uptime_get_32(), overrun, x, y, z);
	}
	/*
	if (IS_ENABLED(CONFIG_LIS2DH_MEASURE_TEMPERATURE)) {
		if (rc == 0) {
			rc = sensor_channel_get(sensor, SENSOR_CHAN_DIE_TEMP, &temperature);
			if (rc < 0) {
				printk("\nERROR: Unable to read temperature:%d\n", rc);
			} else {
				//printk(", t %f\n", sensor_value_to_double(&temperature));
			}
		}
	} else {
		//printk("\n");
	}
	*/
}

void button_pressed(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins)
{
	int val = gpio_pin_get_dt(&button);
	printk("Button pressed at %" PRIu32 ", val: %d\n", k_cycle_get_32(), val);
}

static void bas_notify(void)
{
	uint8_t battery_level = bt_bas_get_battery_level();

	battery_level--;

	if (!battery_level) {
		battery_level = 100U;
	}

	bt_bas_set_battery_level(battery_level);
}

const struct device *const lis_sensor = DEVICE_DT_GET_ANY(st_lis2dh);

ssize_t read_accel_x(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset)
{
	return bt_gatt_attr_read(conn, attr, buf, len, offset, accel_xyz, sizeof(accel_xyz));
}

ssize_t read_accel_y(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset)
{
	return bt_gatt_attr_read(conn, attr, buf, len, offset, &accel_xyz[1], sizeof(accel_xyz[1]));
}

ssize_t read_accel_z(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset)
{
	return bt_gatt_attr_read(conn, attr, buf, len, offset, &accel_xyz[2], sizeof(accel_xyz[2]));
}

static void acc_notify(void)
{
	acc_fetch(lis_sensor);

	if (acc_notify_enabled[0])
		bt_gatt_notify(NULL, &boksbol_attrs.attrs[1], accel_xyz, sizeof(accel_xyz));
	if (acc_notify_enabled[1])
		bt_gatt_notify(NULL, &boksbol_attrs.attrs[4], &accel_xyz[1], sizeof(accel_xyz[1]));
	if (acc_notify_enabled[2])
		bt_gatt_notify(NULL, &boksbol_attrs.attrs[7], &accel_xyz[2], sizeof(accel_xyz[2]));
}

int main(void)
{
	int ret;
	int button_state = 0;

	if (IS_ENABLED(CONFIG_LOG_BACKEND_RTT)) {
		/* Give RTT log time to be flushed before executing tests */
		k_sleep(K_MSEC(1000));
	}

	if (!gpio_is_ready_dt(&button)) {
		printk("Error: button device %s is not ready\n",
		       button.port->name);
		return 0;
	}

	ret = gpio_pin_configure_dt(&button, GPIO_INPUT);
	if (ret != 0) {
		printk("Error %d: failed to configure %s pin %d\n",
		       ret, button.port->name, button.pin);
		return 0;
	}

	ret = gpio_pin_interrupt_configure_dt(&button,
					      GPIO_INT_EDGE_TO_ACTIVE);
	if (ret != 0) {
		printk("Error %d: failed to configure interrupt on %s pin %d\n",
			ret, button.port->name, button.pin);
		return 0;
	}

	gpio_init_callback(&button_cb_data, button_pressed, BIT(button.pin));
	gpio_add_callback(button.port, &button_cb_data);

	if (!pwm_is_ready_dt(&led_red) ||
	    !pwm_is_ready_dt(&led_green) ||
	    !pwm_is_ready_dt(&led_blue)) {
		printk("Error: one or more PWM devices not ready\n");
		return 0;
	}

	if (lis_sensor == NULL) {
		printk("No device found\n");
		return 0;
	}

	if (!device_is_ready(lis_sensor)) {
		printk("Device %s is not ready\n", lis_sensor->name);
		return 0;
	}

	if (IS_ENABLED(CONFIG_BT)) {
		int err;

		bt_conn_cb_register(&conn_callbacks);

		err = bt_enable(NULL);
		if (err) {
			printk("Bluetooth init failed (err %d)\n", err);
			return 0;
		}

		printk("Bluetooth initialized\n");
		//bt_gatt_service_register(&boksbol_attrs);
		err = bt_le_adv_start(adv_param, ad, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
		if (err) {
			printk("Advertising failed to start (err %d)\n", err);
			return 0;
		}

		printk("Beacon started\n");
	}

	printk("Polling at 0.5 Hz\n");
	while (true) {
		acc_notify();
		k_sleep(K_MSEC(1));
	}
}
