"""
Adapted code from https://stackoverflow.com/questions/67172127/real-time-plotting-of-ble-data-using-bleak-and-pyqtgraph-packages
Use only for testing, not production code!
""" 

import asyncio
import struct

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np

from bleak import BleakClient, BleakGATTCharacteristic

from asyncqt import QEventLoop, asyncSlot

# BLE peripheral ID
#address = "12341523-1234-EFDE-1523-785fEABCD123"
address = "E8:6D:C2:9F:79:6F"
UUID_ACC_X = "36010000-1234-EFDE-7001-49E5554DC34C"
UUID_ACC_Y = "36010000-1234-EFDE-7002-49E5554DC34C"
UUID_ACC_Z = "36010000-1234-EFDE-7003-49E5554DC34C"

pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')

class Window(pg.GraphicsLayoutWidget):
    def __init__(self, loop=None, parent=None):
        super().__init__(parent)
        self._loop = loop


        self.setWindowTitle("pyqtgraph example: Scrolling Plots")
        plot = self.addPlot()
        self._data_x = np.zeros(1000)
        self._data_y = np.zeros(1000)
        self._data_z = np.zeros(1000)
        self._curve_x = plot.plot(self._data_x)
        self._curve_y = plot.plot(self._data_y)
        self._curve_z = plot.plot(self._data_z)
        self._client = BleakClient(address, loop=self._loop)

    @property
    def client(self):
        return self._client

    async def start(self):
        await self.client.connect()
        await self.client.start_notify(UUID_ACC_X, self.notify_callback)
        #self.start_read()

    async def stop(self):
        await self.client.disconnect()

    @property
    def data(self):
        return self._data

    @property
    def curve(self):
        return self._curve

    async def read(self):
        data = await self.client.read_gatt_char(UUID_ACC_X)
        #data_y = await self.client.read_gatt_char(UUID_ACC_Y)
        #data_z = await self.client.read_gatt_char(UUID_ACC_Z)

        x,y,z = struct.unpack('ddd', data)
        #y = struct.unpack('d', data_y)[0]
        #z = struct.unpack('d', data_z)[0]

        self.update_plot(x, y, z)
        QtCore.QTimer.singleShot(10, self.start_read)

    def notify_callback(self, sender: BleakGATTCharacteristic, data: bytearray):
        #print("Data: {}".format(data))
        x,y,z = struct.unpack('ddd', data)
        self.update_plot(x, y, z)

    def start_read(self):
        asyncio.ensure_future(self.read(), loop=self._loop)

    def update_plot(self, x, y, z):
        self._data_x[:-1] = self._data_x[1:]
        self._data_x[-1] = x

        self._data_y[:-1] = self._data_y[1:]
        self._data_y[-1] = y

        self._data_z[:-1] = self._data_z[1:]
        self._data_z[-1] = z

        self._curve_x.setData(self._data_x, pen='r', name="X")
        self._curve_y.setData(self._data_y, pen='g', name="Y")
        self._curve_z.setData(self._data_z, pen='b', name="Z")

    def closeEvent(self, event):
        super().closeEvent(event)
        asyncio.ensure_future(self.client.disconnect(), loop=self._loop)


def main(args):
    app = QtGui.QApplication(args)
    loop = QEventLoop(app)
    asyncio.set_event_loop(loop)

    window = Window()
    window.show()

    with loop:
        asyncio.ensure_future(window.start(), loop=loop)
        loop.run_forever()


if __name__ == "__main__":
    import sys

    main(sys.argv)
