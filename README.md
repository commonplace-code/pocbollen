# BoksBoll PoC

A simple minimal test application using NRF52832

* Board devicetree: [nrf52832_cpdev_bb01.dts](https://gitlab.com/commonplace-code/pocbollen/-/blob/main/boards/arm/nrf52832_cpdev_bb01/nrf52832_cpdev_bb01.dts)
* Board overlay: [nrf52832_cpdev_bb01-pinctrl.dtsi](https://gitlab.com/commonplace-code/pocbollen/-/blob/main/boards/arm/nrf52832_cpdev_bb01/nrf52832_cpdev_bb01-pinctrl.dtsi)
* Application: (Peripheral application)[https://gitlab.com/commonplace-code/pocbollen/-/tree/main/application]
* Graphing: (Central graphing PoC)[https://gitlab.com/commonplace-code/pocbollen/-/tree/main/central]

## BLE implementation
The NRF52832 runs as a perpipheral GATT server. The available services and characteristics:

* BoksBol service: `12341523-1234-EFDE-1523-785fEABCD123`
* ACC X/Y/Z characteristic: `36010000-1234-EFDE-7001-49E5554DC34C`, 
  * perimissions: `Read | Notify`
  * Data format: `<double x><double y><double z>`

## Central graphing application
Python graphing test based on [Bleak](https://bleak.readthedocs.io/en/latest/) and [PyQTGraph](https://www.pyqtgraph.org/)

The code is an adjusted example from https://stackoverflow.com/questions/67172127/real-time-plotting-of-ble-data-using-bleak-and-pyqtgraph-packages

![Plot screenshot](docs/acc_data2.png "Accelerometer data capture")